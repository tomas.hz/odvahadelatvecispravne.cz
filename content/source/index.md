---
layout: default
---

<div id="header-wrapper">
	<h1>Volební program kandidátky</h1>
</div>

## ![](assets/images/bydleni.png) Dostupné a důstojné bydlení pro kvalitní život

*Město tvoří lidé a lidé jsou jeho největším pokladem. Kvalitní a dostupné bydlení je základním smyslem rozvoje města. Vrátíme pozornost zpět k lidem a pomůžeme jim.*

- Představíme jasnou bytovou politiku města včetně plánu rozvoje bytového fondu a zastavíme nevýhodné rozprodávání městského majetku předvybraným developerům.
- Zasadíme se o nárůst počtu obyvatel usnadněním výstavby pro lidi, kteří chtějí ve Volarech žít, a zároveň vytvoříme podmínky, kdy nebudou developeři upřednostňováni na úkor občanů a města. Budeme velmi pečlivě kontrolovat záměry a způsob provádění každé developerské výstavby, aby neničila přirozený vzhled a místní život ve Volarech, místních částech i na samotách. V odůvodněných případech nebudeme váhat zakročit a zabránit vzniku trvale nenapravitelných škod, které by mohly být způsobeny.
- Budeme rozšiřovat městský bytový fond o běžné i startovací byty, město na tom vydělá a pomůže svým občanům dostupně bydlet.
- Podpoříme vznik nových domů s pečovatelskou službou pro udržení důstojné kvality života a zlepšení podmínek pro seniory, zasadíme se o rekonstrukci domu č. 145 k tomuto účelu.
- Začneme spolupracovat s městským architektem, který pomůže uchytit unikátní koncepci Volar jako vesnického městského sídla a brány Šumavy.
- Dokončíme urychleně schvalování územního plánu a okamžitě zahájíme práce na nové kvalitnější verzi, aby byl přijat do konce volebního období, a zabezpečil rozvoj celého města alespoň na deset let dopředu.
- Využijeme každou stavební akci pro rozšiřování vysokorychlostního internetu na území města, prioritně optickými kabely, podpoříme jeho budování i v odlehlých částech a samotách, abychom zvýšili dostupnost rychlé sítě pro všechny občany.

## ![](assets/images/otevrena-radnice.png) Otevřená a moderní radnice

*Město slouží lidem a ne lidé městu. Otevřený úřad má být výkladní skříní, díky které má každý občan možnost sledovat transparentní a profesionální správu města a na vlastní oči si vyzkoušet, že procesy fungují, jak mají, a město je jeho partnerem.*

- Otevřeme jednání zastupitelstva pro všechny, zajistíme živé přenosy jednání a poskytneme materiály do zastupitelstva veřejně všem občanům.
- Hlasování Rady bude po jménech, už ne anonymně, a zápisy z Rady i Zastupitelstva budou podrobné a úplné, aby bylo jasné o čem se jednalo.
- Odmítneme každý střet zájmů i korupci a při jejich odhalení budeme požadovat okamžité odstoupení daných osob z volené funkce.
- Zapojíme město a všechny jeho organizace do Registru smluv, kde se začnou zveřejňovat všechny smlouvy uzavřené v hodnotě od 50.000 Kč nahoru.
- Provedeme inventuru městského majetku a jeho užívání. Provedeme revizi starých nájemních a pachtovních smluv za účelem hospodárné správy městského majetku.
- Otevřeme město spolupráci s vysokými školami i jinými institucemi a budeme usilovat o partnerství s nimi, aby město získalo a využilo analyzované big data třeba o dopravě, školství nebo životním prostředí.
- Změníme pravidla pro zadávání veřejných zakázek, tak aby bylo dosaženo vyššího množství uchazečů za účelem snižování cen. Skončíme s praxí, kdy nejsou soutěženy všechny stavební zakázky nad 6 milionů korun na veřejném portálu zadavatele. Stejně budeme postupovat i u městských příspěvkových organizací a firem.
- Zajistíme ziskové fungování hotelu Bobík, i pokud by to znamenalo změnu modelu jeho provozování. Postaráme se o transparentní hospodaření Městských lesů.
- Veškerá výběrová řízení na pozice ředitelů a jednatelů budou probíhat před nezávislou výběrovou komisí a pozice budou aktivně inzerovány na pracovních portálech, aby bylo dosaženo co nejvyššího počtu kvalitních uchazečů a skoncováno s praxí předvybraných uchazečů.

## ![](assets/images/zivotni-prostredi.png) Péče o životní prostředí začíná tam, kde žijeme

*Příroda tu bude, i když my tu nebudeme. Prostředí kolem nás je základem spokojeného a klidného života. Úkolem města je tomu pomáhat a vycházet vstříc.*

- Nebudeme ukončovat pronájmy zahrádek a zaručíme dlouhodobé pokračování jejich nájmů stávajícím nájemcům.
- Budeme zlepšovat podmínky pro kvalitní život ve Volarech, který se pozitivně projeví i na zastavení poklesu obyvatel města.
- Snížíme poplatky za komunální odpad, protože ve spolupráci s občany dosáhneme snížení celkového množství odpadu, zapojíme občany i návštěvníky města do procesu druhotného využití odpadu, navýšení podílu recyklace a zajistíme odpovídající finanční zapojení všech účastníků systému odpadového hospodářství včetně podnikatelů a pronajímatelů.
- Zajistíme, aby město připravilo proces na opravu a financování ČOV, který splní. Přestaneme s litím peněz do kanálu za neustálé opravy a připravíme plán rekonstrukce a financování nevyhovujícího vodovodního řadu a zahájíme jeho plnění.
- Podpoříme trvale udržitelné hospodaření městských společností a budeme stejné nároky klást i na městské pachtýře.
- Zahájíme proces zvýšení energetické samostatnosti městských budov a zvyšování podílu modrozelené infrastruktury.

## ![](assets/images/zdravotni-pece.png) Dostupná a kvalitní zdravotní péče

*Zdraví máme jen jedno. Dnes už ale není jedno, odkud člověk pochází, aby nebyl znevýhodněn ve zdravotní péči. Posílíme dostupnost i kvalitu zdravotní péče pro Volary.*

- Podpoříme dostupnost zdravotnických služeb praktického lékaře i stomatologa pro všechny občany. Aktivně budeme vyhledávat a podporovat mladé lékaře k založení ordinace ve Volarech.
- Podpoříme terénní zdravotnické i jiné podpůrné služby na území města.
- Podpoříme prevenci, aby se předcházelo vzniku návykového chování a omezíme tak škodlivé jevy, které způsobuje.

## ![](assets/images/aktivni-verejnost.png) Vstříc aktivní veřejnosti

*Jen veřejná diskuse umožňuje rovnocenný vztah občanů a města a jen rovnocenný vztah posiluje aktivitu občanů, která je motorem rozvoje města.*

- Vytvoříme osadní výbory pro každou městskou část i oblast města, kde o to občané projeví zájem. Osadní výbor bude přirozeným partnerem města v diskusi o chystaných prodejích, stavbách i dalších událostech plánovaných na území daného sídla.
- O zásadních věcech uspořádáme místní referenda pro občany.
- Připravíme městský elektronický portál pro zpětnou vazbu a diskusi s občany.
- Podpora místního podnikání vytváří pracovní příležitosti pro občany Volar a pomáhá jeho ekonomickému rozvoji. Vytvoříme koncepci pobídek pro příchozí firmy, které navýší dostupnost služeb a pracovních míst.
- Budeme podporovat činnost místních spolků i vznik nových. Pomůžeme jim nejen finančně ale i městskými prostory a zázemím.

## ![](assets/images/doprava.png) Lepší dopravní obslužnost

*Volary jsou hlavní křižovatkou jižní Šumavy. Město tuto situaci potřebuje využít ve prospěch občanů a přitom minimalizovat negativní dopady, které doprava způsobuje.*

- Zasadíme se o zklidnění dopravy a budeme se soustředit na vybudování obchvatů města, které svedou dopravu mimo centrum.
- Zpomalíme dopravu v obydlených oblastech, zajistíme hospodárné využití radarů a vytvoření zklidňujících prvků dopravy.
- Dobudujeme napojení na okruh cyklistických tras, napojíme stávající síť na Chlum a Magdalenu, dokončíme pokračování stezky na Soumarském mostě a na Bobík. Tak zvýšíme možnosti využití cyklodopravy pro obyvatele i návštěvníky.
- Budeme aktivně hájit návaznost jednotlivých složek veřejné dopravy ve městě.

## ![](assets/images/skolstvi.png) Kvalitní školství a vzdělávání

*Školy a školky nejsou jen odkladištěm dětí, aby rodiče mohli jít do práce, ale naprosto zásadní součástí naší komunity, která ve spolupráci s nimi roste nebo strádá. Kvalitní škola přitahuje nové obyvatele a špatná je odrazuje. Pojďme naproti té dobré.*

- Škola a školky jsou jeden ze tří stavebních kamenů obce. Zvýšíme jejich kvalitu a prosadíme výběr kvalitních ředitelů transparentními výběrovými řízení, aby je vedly pedagogičtí lídři a schopní manažeři.
- Aktivně pomůžeme získávání nových učitelů i díky služebním bytům a budeme podporovat rozvoj mateřských školek včetně lesní školky.
- Zvýšíme kvalitu výuky a poměr pedagogicky vzdělaných učitelů.
- Půjdeme cestou digitalizace a snižování administrativy.
- Zasadíme se o vznik Volarské iQLANDIE – místa s praktickou aplikací přírodních věd zaměřenou na vědu hrou a školskou turistiku.

## ![](assets/images/kultura.png) Rozvoj kultury a podpora sportu

*Nejen chlebem je člověk živ a zdravý duch se rozvíjí jen ve zdravém těle. Kultura i sport je zároveň místem setkávání lidí a výměny zážitků a zkušeností. Pokračujme v dlouhodobé tradici kulturních akcí a sportovních činností.*

- Budeme rozvíjet stávající úspěšné kulturní akce na území města a podpoříme přeshraniční rozvoj vztahů.
- Budeme podporovat sportovní zařízení a práci sportovních klubů se zaměřením na podporu dětí a mládežnického sportu.
- Podpoříme modernizaci knihovny, aby sloužila jako nízkoprahové místo pro přístup k informacím.

## ![](assets/images/cestovni-ruch.png) Udržitelný cestovní ruch

*Host do domu, bůh do domu. Třetí den však ryba smrdí od hlavy. Pro město je klíčová oblast rozvoje cestovní ruch, který nesmí být prováděn na úkor občanů ale v souladu s nimi a v jejich prospěch. Jen tak naplní trvale udržitelným způsobem svůj potenciál a pomůže všem obyvatelům našeho města.*

- Připravíme koncepci trvale udržitelného rozvoje území s vysokým návštěvnickým potenciálem tak, aby byly vytvořeny podmínky pro rozvoj cestovního ruchu a zároveň nedocházelo k narušování životní pohody obyvatel Volar.
- Vybudujeme scházející návštěvnickou infrastrukturu jako třeba parkoviště na Soumarském mostě a necháme ji vydělat na své pořízení a provoz.
- Zaměříme se na propagaci a využití městského majetku jako například plaveckého bazénu, muzea a dalších, které poslouží jako lákadla pro návštěvu města.
- Zjednodušíme výběr místních poplatků zavedením elektronických nástrojů jejich výběru.
- Městský hotel Bobík bude fungovat v souladu s koncepcí udržitelného rozvoje cestovního ruchu.
- Zapojíme se do přeshraničních aktivit a navážeme spolupráci na rozvoji turismu i s okolními obcemi.

## ![](assets/images/bezpecne-mesto.png) Bezpečné město

*Město není biřic ani dráb, ale je první institucí, která navazuje důvěru veřejnosti ve spravedlivé a rovné zacházení a zvyšuje důvěru svých obyvatel v bezpečný a klidný život.*

- Prohloubíme program Asistentů prevence kriminality a zavedeme dle potřeby občasnou výpomoc s Městskou policií z Prachatic nebo Vimperku.
- Zasadíme se o rozvoj spolupráce s Policí České Republiky.
- Postavíme se ale vůči nadbytečnému sledování občanů v případech, kdy to není opodstatněné.
